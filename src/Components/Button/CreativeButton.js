import React from 'react'
import PropTypes from 'prop-types';
import '../../Styles/Button/CreativeButton.css';

export default function CreativeButton() {
    return (
        <a id='creative-btn' href='/'>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            @tungxd96
        </a>
    )
}

CreativeButton.propTypes = {
    title: PropTypes.string,
}

CreativeButton.defaultProps = {
    title: 'TD',
}