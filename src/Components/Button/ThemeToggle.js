import React from 'react'
import '../../Styles/Button/ThemeToggle.css';

export default function ThemeToggle() {
    function toggleTheme() {
        const container = document.querySelector('div[id=\'container\']');
        container.classList.toggle('light');
    }

    return (
        // <input
        //     id='theme-toggle'
        //     type='checkbox'
        //     onClick={toggleTheme}
        // />
        <button id='theme-toggle' onClick={toggleTheme}>
            <i className="fa fa-lightbulb" aria-hidden='true'></i>
        </button>
    )
}
