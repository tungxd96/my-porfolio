import React from 'react';
import '../../Styles/Button/NeumorphismButton.css';

export default function NeumorphismButton() {
    return (
        <div className='input-box'>
            <input
                type='submit'
                name=''
                value='Submit'
            />
        </div>
    )
}
