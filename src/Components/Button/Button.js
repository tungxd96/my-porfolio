import React from 'react';
import '../../Styles/Button/Button.css';
import PropTypes from 'prop-types';

function Button(props) {
    const { href, value, onClick, style } = props;
    return (
        <a
            className='action-button'
            href={href}
            onClick={() => onClick()}
            style={style}
        >{value}</a>
    );
}

Button.propTypes = {
    href: PropTypes.string,
    value: PropTypes.string,
    onClick: PropTypes.func,
    style: PropTypes.object
}

Button.defaultProps = {
    href: '#',
    value: 'Button',
    onClick: () => { },
    style: {}
}

export default Button;