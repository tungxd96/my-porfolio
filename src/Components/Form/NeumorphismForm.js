import React from 'react';
import '../../Styles/Form/NeumorphismForm.css';
import PropTypes from 'prop-types';

export default function NeumorphismForm(props) {
    function renderHeading() {
        if (!props.heading) {
            return null;
        }

        return (
            <h2>{props.heading}</h2>
        );
    }

    return (
        <div className='neumorphism-form'>
            {renderHeading()}
            {props.children}
        </div>
    )
}

NeumorphismForm.propTypes = {
    heading: PropTypes.string,
}

NeumorphismForm.defaultProps = {
    heading: null,
}