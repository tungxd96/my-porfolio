import React, { useState } from 'react'
import '../../Styles/Form/Form.css'
import emailjs from 'emailjs-com'

export default function Form() {
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [subject, setSubject] = useState('')
    const [message, setMessage] = useState('')

    function handleChange(e, type) {
        switch (type) {
            case 'name':
                setName(e.target.value)
                break
            case 'email':
                setEmail(e.target.value)
                break
            case 'subject':
                setSubject(e.target.value)
                break
            case 'message':
                setMessage(e.target.value)
                break
            default:
                break
        }
    }

    function resetInput() {
        setName('')
        setEmail('')
        setSubject('')
        setMessage('')
    }

    function sendEmail(e) {
        e.preventDefault()

        var templateParams = {
            name: name,
            email: email,
            subject: subject,
            message: message
        }

        emailjs.send('porfolio', 'porfolio_template', templateParams, 'user_r0iOw2cIbd0rprRQCxfI3')
            .then(function (response) {
                console.log('SUCCESS!', response.status, response.text);
                resetInput()
            }, function (error) {
                console.log('FAILED...', error);
            });
    }

    return (
        <form onSubmit={sendEmail} className='contact-form'>
            <div className='form-row'>
                <input
                    type='text'
                    placeholder='Name'
                    name='name'
                    value={name}
                    onChange={(e) => handleChange(e, 'name')}
                    required={true}
                />
                <input
                    type='email'
                    placeholder='Email'
                    name='email'
                    value={email}
                    onChange={(e) => handleChange(e, 'email')}
                    required={true}
                />
            </div>
            <input
                id='subject'
                type='text'
                placeholder='Subject'
                name='subject'
                value={subject}
                onChange={(e) => handleChange(e, 'subject')}
            />
            <textarea
                placeholder='Type your message...'
                name='message'
                value={message}
                onChange={(e) => handleChange(e, 'message')}
                required={true}
            />
            <input
                type='submit'
                value='Send'
            />
        </form>
    )
}
