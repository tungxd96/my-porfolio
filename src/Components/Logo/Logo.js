import React from 'react';
import '../../Styles/Logo/Logo.css';
import CreativeButton from '../Button/CreativeButton';

export default function Logo() {
    return (
        <header>
            <CreativeButton title='TD' />
        </header>
    )
}
