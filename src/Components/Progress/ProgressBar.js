import React, { useEffect } from 'react'
import '../../Styles/Progress/ProgressBar.css';

export default function ProgressBar() {
    useEffect(() => {
        setTimeout(() => {
            const progress = document.getElementById('progressbar');
            if (progress) {
                progress.remove();
            }
        }, 2000);
    }, []);

    return (
        <div id='progressbar' className='progressbar'>
            <div className='text'>
                <span>Loading</span>
                <span>.</span>
                <span>.</span>
                <span>.</span>
            </div>
        </div>
    )
}
