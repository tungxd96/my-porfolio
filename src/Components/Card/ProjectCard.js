import React from 'react'
import '../../Styles/Card/ProjectCard.css'
import PropTypes from 'prop-types'
import { ICONS } from '../../Constants/Icons';
export default function ProjectCard(props) {
    // function onLearnMore(e) {
    //     e.preventDefault()
    //     window.location.href = props.href
    // }
    const HOLDER_SIZE = '33.3vw';

    const {
        title,
        style,
        index,
        link,
        description = '',
        projectKey
    } = props;

    const renderCoverImage = ({
        imageStyle = {}
    } = {}) => (
        <div
            className='project-holder'
            style={{ width: HOLDER_SIZE, height: HOLDER_SIZE }}
        >
            {ICONS[projectKey] ? (
                <img
                    src={ICONS[projectKey]}
                    alt={title}
                    width='80%'
                    height='80%'
                    style={{
                        objectFit: 'cover',
                        objectPosition: '15%',
                        ...imageStyle
                    }}
                />
            ) : null}
        </div>
    );

    const renderContent = ({
        style = {}
    } = {}) => (
        <div
            className='project-card-title'
            style={{
                width: '50%',
                marginLeft: '5vw',
                ...style
            }}
        >
            {title}
            <div className='project-card-description'>
                {description}
            </div>
            <a
                className="project-card-description bold"
                href={link}
            >
                {'Demo \u276F'}
            </a>
        </div>
    );

    if (index % 2 === 1) {
        return (
            <div className='project-card-container' style={style}>
                {renderCoverImage()}
                {renderContent()}
            </div>
        );
    }
    return (
        <div className='project-card-container' style={style}>
            {renderContent({
                style: { marginRight: '5vw', marginLeft: 0 }
            })}
            {renderCoverImage()}
        </div>
    );
}

ProjectCard.propTypes = {
    title: PropTypes.string,
    style: PropTypes.object,
    index: PropTypes.number
}

ProjectCard.defaultProps = {
    title: null,
    style: {},
    index: 0
}