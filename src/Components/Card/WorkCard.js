import React from 'react';
import PropTypes from 'prop-types';
import '../../Styles/Card/WorkCard.css';

// const ICON_SIZE = 200;
const WorkCard = props => {
    const {
        employer,
        // index,
        style,
        // onClick,
        period: { from, to } = {},
        descriptions,
        pills
    } = props;
    // const getIndex = () => {
    //     if (index < 10) {
    //         return `0${index}`
    //     }
    //     return index.toString();
    // }
    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'space-between',
                ...style
            }}
        >
            <div className='work-title' style={{ width: '35%' }}>
                {employer}
                <div className='work-period' style={{ marginTop: '0.8vh' }}>
                    {`${from} - ${to}`}
                </div>
            </div>
            <div style={{ width: '65%' }}>
                {descriptions.map((description, index) => (
                    <div
                        key={`${employer}-description-${index}`}
                        className='work-description'
                        style={{ display: 'flex' }}
                    >
                        <div>{'\u276F'}</div>
                        <div style={{ marginLeft: '1vw', textAlign: 'justify' }}>{description}</div>
                    </div>
                ))}
                <div style={{ display: 'flex', marginTop: '0.8vh', overflow: 'auto' }}>
                    {pills.map((data, index) => (
                        <div
                            key={index}
                            style={index !== 0 ? { marginLeft: '1vw' } : {}}
                            className='work-pill'
                        >
                            {data}
                        </div>
                    ))}
                </div>
            </div>
        </div>
        // <div
        //     className='work-card-container'
        //     style={style}
        //     onClick={() => onClick()}
        // >
        //     <div className='work-card-employer'>
        //         {`${getIndex()} ${employer}`}
        //     </div>
        //     <div className="work-card-icon-container">
        //         {props.icon && typeof props.icon === 'function' ? (
        //             <props.icon width={ICON_SIZE} height={ICON_SIZE} />
        //         ) : (
        //             <div>
        //                 {employer}
        //             </div>
        //         )}
        //     </div>
        // </div>
    );
}

WorkCard.propTypes = {
    employer: PropTypes.string,
    title: PropTypes.string,
    icon: PropTypes.func,
    index: PropTypes.number,
    style: PropTypes.object,
    onClick: PropTypes.func,
    pills: PropTypes.array,
    descriptions: PropTypes.array
};

WorkCard.defaultProps = {
    employer: 'Example',
    title: 'Example',
    icon: () => null,
    index: 1,
    style: {},
    onClick: () => { },
    pills: [],
    descriptions: []
};

export default WorkCard;