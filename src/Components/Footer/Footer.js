import React from 'react';
import '../../Styles/Footer/Footer.css';

const DESIGNED_BY = 'Pooja Sinha'
const DESIGNED_BY_LINK = 'https://www.behance.net/poojasinhaa';
const Footer = props => {
    return (
        <div className='footer-container'>
            <div className='footer-text'>
                <div>
                    <span>Designed by </span>
                    <a
                        href={DESIGNED_BY_LINK}
                        style={{
                            color: '#999999', cursor: 'pointer'
                        }}
                    >{DESIGNED_BY}</a>
                </div>
                <div style={{ textAlign: 'center', marginTop: '0.8vw' }}>
                    {`Tung Dinh \u2022 2021`}
                </div>
            </div>
        </div>
    );
}

export default Footer;