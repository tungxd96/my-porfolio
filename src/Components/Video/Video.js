import React from 'react';
import '../../Styles/Video/Video.css';

export default function Video(props) {
    const [isVideoLoaded, setIsVideoLoaded] = React.useState(false);

    var keys = { 37: 1, 38: 1, 39: 1, 40: 1 };
    function onLoadedData() {
        setIsVideoLoaded(true);
    };

    function preventDefault(e) {
        e.preventDefault();
    }

    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }

    // modern Chrome requires { passive: false } when adding event
    var supportsPassive = false;
    try {
        window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
            get: function () { supportsPassive = true; }
        }));
    } catch (e) { }

    var wheelOpt = supportsPassive ? { passive: false } : false;
    var wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';

    // call this to Disable
    function disableScroll() {
        window.addEventListener('DOMMouseScroll', preventDefault, false); // older FF
        window.addEventListener(wheelEvent, preventDefault, wheelOpt); // modern desktop
        window.addEventListener('touchmove', preventDefault, wheelOpt); // mobile
        window.addEventListener('keydown', preventDefaultForScrollKeys, false);
    }

    // call this to Enable
    function enableScroll() {
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.removeEventListener(wheelEvent, preventDefault, wheelOpt);
        window.removeEventListener('touchmove', preventDefault, wheelOpt);
        window.removeEventListener('keydown', preventDefaultForScrollKeys, false);
    }

    window.addEventListener('scroll', function () {
        const layer = document.getElementById('home-layer');
        const video = document.getElementById('home-mp4')
        const blur = (window.scrollY / 600) * 6;
        const opacity = ((window.scrollY / 600) / 1.5) * 0.4 + 0.3;
        if (layer) {
            layer.style.opacity = opacity;
            video.style.filter = `blur(${blur}px)`
        }
    })

    return (
        <div className='banner'>
            <video
                id='home-mp4'
                autoPlay
                muted
                loop
                onLoadedData={onLoadedData}
            >
                <source
                    src='https://s3.us-east-2.amazonaws.com/tungxdinh.com/src_Components_Video_home.mp4'
                    type='video/mp4'
                />
            </video>
            <div className='content'>
                {props.children}
            </div>
        </div>
    )
}
