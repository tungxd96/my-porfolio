import React, { useEffect, useState } from 'react';
import '../../Styles/Navbar/Navbar.css';
import Button from '../Button/Button';
import socialLinksMap from '../../Resources/social-links.json';

export const getSocialLinks = () => socialLinksMap || [];

export default function Navbar(props) {
    const socialLinks = getSocialLinks();
    const [cvWidth, setCVWidth] = useState(0);
    const [cvHeight, setCVHeight] = useState(0);
    // const toggleNavUnderline = (key = 'Work', isSelected = true) => {
    //     const navLine = document.getElementById(key + 'NavUnderline');
    //     if (isSelected) {
    //         navLine.style.backgroundColor = "black";
    //     }
    //     else {
    //         navLine.style.backgroundColor = "transparent";
    //     }
    // }

    useEffect(() => {
        // document.addEventListener('scroll', () => {
        //     const boxY = window.scrollY / (window.innerHeight * 3);
        //     const boxShadowOpacity = boxY >= 0.1 ? 0.1 : boxY;
        //     const navbarHeader = document.getElementById('NavbarHeader');
        //     if (!navbarHeader) return;
        //     navbarHeader.style.boxShadow = `-1px -1px 3px rgba(255, 255, 255, 0.5), 2px 2px 6px rgba(0, 0, 0, ${boxShadowOpacity})`;
        //     if (window.scrollY < window.innerHeight - OFFSET) {
        //         toggleNavUnderline('Work', false);
        //         toggleNavUnderline('Project', false);
        //         toggleNavUnderline('Contact', false);
        //     }
        //     else if (window.scrollY >= window.innerHeight - OFFSET
        //         && window.scrollY < 2 * window.innerHeight - OFFSET) {
        //         toggleNavUnderline('Work');
        //         toggleNavUnderline('Project', false);
        //         toggleNavUnderline('Contact', false);
        //     }
        //     else if (window.scrollY >= 2 * window.innerHeight - OFFSET
        //         && window.scrollY < 3 * window.innerHeight - OFFSET) {
        //         toggleNavUnderline('Work', false);
        //         toggleNavUnderline('Project');
        //         toggleNavUnderline('Contact', false);
        //     }
        //     else if (window.scrollY >= 3 * window.innerHeight - OFFSET) {
        //         toggleNavUnderline('Work', false);
        //         toggleNavUnderline('Project', false);
        //         toggleNavUnderline('Contact');
        //     }
        // })
        const ro = new ResizeObserver(entries => {
            for (const entry of entries) {
                setCVHeight(entry.target.clientHeight);
                setCVWidth(entry.target.clientWidth);
            }
        })
        ro.observe(document.getElementById('download-cv'));
    }, []);

    const scrollTo = (id) => {
        window.scroll(0, document.getElementById(id).offsetTop);
    }

    return (
        <section
            id='Navbar'
            style={{
                paddingLeft: '11vw',
                paddingRight: 0,
                justifyContent: 'center',
                alignItems: 'center'
            }}
        >
            <div
                style={{
                    backgroundColor: 'transparent',
                    justifyContent: 'flex-end',
                    display: 'flex',
                    padding: '4.5vh 0',
                    paddingRight: '8vw',
                    position: 'absolute',
                    top: 0,
                    right: 0,
                    zIndex: 100
                }}
            >
                <div
                    className="nav-btn"
                    onClick={() => scrollTo('Experience')}
                    style={{ marginRight: '3.8vw' }}
                >
                    Work
                </div>
                <div
                    className="nav-btn"
                    onClick={() => scrollTo('Project')}
                    style={{ marginRight: '3.8vw' }}
                >
                    Project
                </div>
                <div
                    className="nav-btn"
                    onClick={() => scrollTo('About')}
                >
                    About
                </div>
            </div>
            <div style={{ display: 'flex' }}>
                <div style={{ position: 'relative' }}>
                    <div
                        style={{
                            color: '#999999',
                            fontWeight: '700',
                            fontSize: '6vw',
                            width: '100%',
                            paddingRight: '4vw',
                            zIndex: 10,
                            whiteSpace: 'nowrap'
                        }}
                    >
                        <p>Hello,</p>
                        <p>I'm Tung</p>
                        <p>Dinh</p>
                        <div id='download-cv'>
                            <Button
                                value="Download CV"
                                href='https://s3.us-east-2.amazonaws.com/tungxdinh.com/tung_resume+(2).pdf'
                            />
                        </div>
                    </div>
                    <div style={{
                        backgroundColor: '#D8D8D8',
                        opacity: 0.22,
                        mixBlendMode: 'normal',
                        position: 'absolute',
                        width: '100vw',
                        height: '100vh',
                        zIndex: -1,
                        bottom: cvHeight / 3,
                        left: cvWidth / 3.6
                    }} />
                </div>
                <div style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingRight: '11vw'
                }}>
                    <div
                        style={{
                            borderLeft: '1px solid #d8d8d8',
                            color: '#6D6D6D',
                            fontSize: '1vw',
                            lineHeight: '2.5vw',
                            letterSpacing: '0.385714px',
                            fontWeight: 'normal',
                            fontStyle: 'normal',
                            paddingLeft: '4vw'
                        }}
                    >
                        <p>
                            I'm working and living in Seattle, WA. Currently, I experienced Full Stack Engineer with a demonstrated history of working in the health wellness and fitness industry. Download the CV to know more about my background. Checkout my profile below.
                        </p>
                        <div style={{
                            display: 'flex',
                            marginTop: '1.8vw',
                            color: 'black',
                            fontStyle: 'normal',
                            fontSize: '0.9vw',
                            letterSpacing: '0.5vw',
                            lineHeight: '2.5vw',
                            fontWeight: '500'
                        }}>
                            {socialLinks.map((data, index) => (
                                <a
                                    key={index}
                                    className='link'
                                    style={index !== 0 ? { marginLeft: '2.5vw' } : {}}
                                    href={data.href}
                                >
                                    {data.name}
                                </a>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}