import React from 'react';
import PropTypes from 'prop-types';
import '../../Styles/TextInput/NeumorphismTextInput.css';

export default function NeumorphismTextInput(props) {
    function renderLabel() {
        if (!props.label) {
            return null;
        }
        return (
            <label>{props.label}</label>
        );
    }

    if (props.type === 'textarea') {
        return (
            <div className='input-box'>
                {renderLabel()}
                <textarea
                    name=''
                    placeholder={props.placeholder}
                />
            </div>
        )
    }

    return (
        <div className='input-box'>
            {renderLabel()}
            <input
                type='text'
                name=''
                placeholder={props.placeholder}
            />
        </div>
    )
}

NeumorphismTextInput.propTypes = {
    label: PropTypes.string,
    placeholder: PropTypes.string,
    type: PropTypes.oneOf(['textinput, textarea'])
}

NeumorphismTextInput.defaultProps = {
    label: null,
    placeholder: null,
    type: 'textinput',
}