import React from 'react'
import '../../Styles/Title/Title.css';
import PropTypes from 'prop-types';

export default function Title(props) {
    return (
        <div className='title' style={props.style}>
            <span>{props.title}</span>
            {/* {!props.delDivider ? (
                <div className='title-divider'></div>
            ) : null} */}
        </div>
    )
}

Title.propTypes = {
    title: PropTypes.string,
}

Title.defaultProps = {
    title: 'Title',
}