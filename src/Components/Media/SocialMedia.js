import React from 'react'
import '../../Styles/Media/SocialMedia.css'

export default function SocialMedia() {
    return (
        <div className='social-wrapper'>
            <a href='https://www.facebook.com/tungxd96'>
                <i className="fab fa-facebook-f"></i>
            </a>
            <a href='https://twitter.com/tungxdinh'>
                <i className="fab fa-twitter"></i>
            </a>
            <a href='https://www.linkedin.com/in/tung-dinh-01'>
                <i className="fab fa-linkedin-in"></i>
            </a>
            <a href='https://github.com/tungxd96'>
                <i className="fab fa-github"></i>
            </a>
            <a href='https://gitlab.com/tungxd96'>
                <i className="fab fa-gitlab"></i>
            </a>
        </div>
    )
}
