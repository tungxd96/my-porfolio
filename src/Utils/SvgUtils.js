export const getSvgSize = (viewBox = '0 0 1 1', props) => {
    const view = viewBox.split(" ");
    const orgW = parseInt(view[2]);
    const orgH = parseInt(view[3]);
    const { width, height } = props;
    const getSize = () => {
        if (width && height) {
            if (orgW > orgH) {
                return {
                    width: width,
                    height: (width * orgH) / orgW
                }
            }
            else {
                return {
                    width: (orgW * height) / orgH,
                    height: height
                }
            }
        }
        if (width) {
            return {
                width: width,
                height: (width * orgH) / orgW
            }
        }
        if (height) {
            return {
                width: (orgW * height) / orgH,
                height: height
            }
        }
        return { width, height };
    }
    const size = getSize();
    return {
        width: parseInt(size.width),
        height: parseInt(size.height)
    };
}