import workMapFromFile from '../Resources/works-map.json';

export const getWorkMapFromFile = () => {
    return workMapFromFile || {};
}