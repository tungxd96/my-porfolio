import React from 'react';
import Form from '../../Components/Form/Form';
import Title from '../../Components/Title/Title';
import '../../Styles/Screen/Contact/Contact.css';
import Footer from '../../Components/Footer/Footer';

export default function Contact() {
    const myEmail = 'tungxd301@gmail.com';
    return (
        <section id='Contact' className='light'>
            <div className='contact-container'>
                <Title title='Contact' />
                <div className='contact-content'>
                    <div className='contact-text-container'>
                        <div>
                            <span>If you'd like to contact me, send me an email at </span>
                            <span
                                className='contact-email'
                                onClick={e => {
                                    e.preventDefault();
                                    window.open(`mailto:${myEmail}`);
                                }}
                            >{myEmail}</span>
                        </div>
                    </div>
                    <Form />
                </div>
            </div>
            <Footer />
        </section>
    )
}
