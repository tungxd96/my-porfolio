import React from 'react'
import ProjectCard from '../../Components/Card/ProjectCard';
import Title from '../../Components/Title/Title'
import { ICONS } from '../../Constants/Icons';
import '../../Styles/Screen/Project/Project.css';
import projectsMap from '../../Resources/projects-map.json';

// const THRESHOLD = 10;
export const getProjectsMap = () => projectsMap || {};

// const PROJECTS_THRESHOLD = 2;
export default function Project() {
    const projects = getProjectsMap();
    return (
        <section id='Project' className='light'>
            <Title title='Projects' />
            <div className='wrapper'>
                {Object.entries(projects)
                    .sort((a, b) => a[1].order - b[1].order)
                    .map(([projectKey, projectValue], index) => {
                        // if (index >= PROJECTS_THRESHOLD) return null;
                        return (
                            <ProjectCard
                                key={`${projectKey}-${index}`}
                                projectKey={projectKey}
                                index={index + 1}
                                {...projectValue}
                                icon={ICONS[projectKey] || null}
                                style={index === 0 ? {} : { marginTop: '120px' }}
                                onClick={() => console.log(`Press ${projectKey}`)}
                            />
                        );
                    })}
            </div>
        </section>
    )
}

/*
<div className='project-row'>
                    <div data-aos='fade-right' className='project-col'>
                        <ProjectCard
                            languages='React CSS JavaScript'
                            alt='Pathfinding Visualizer'
                            href='https://master.dfybu778glv9v.amplifyapp.com'
                            icon='fas fa-route'
                        >
                            <div className='description'>
                                <li>Programmed A*, Dijkstra, Breadth-first Search, Depth-first Search, and Greedy Best-first Search algorithm to find a path from one node to another</li>
                                <li>Implemented Depth-first Search algorithm to generate maze</li>
                                <li className='text-link'>
                                    Inspired by&ensp;
                                    <a className='link' href='https://www.youtube.com/channel/UCaO6VoaYJv4kS-TQO_M-N_g'>Clément Mihailescu</a>
                                </li>
                            </div>
                        </ProjectCard>
                        <ProjectCard
                            languages='Python Tkinter'
                            alt='Maze Generator & Solver'
                            href='https://s3.us-east-2.amazonaws.com/tungxdinh.com/maze.mp4'
                            icon='fab fa-magento'
                        >
                            <div className='description'>
                                <li>Implemented Depth-first Search algorithm to generate maze</li>
                                <li>Programmed A* algorithm to find a path from one node to another</li>
                            </div>
                        </ProjectCard>
                    </div>
                    <div data-aos='fade-right' className='project-col'>
                        <ProjectCard
                            languages='Python'
                            alt='Sorting Algorithm'
                            href='https://gist.github.com/tungxd96/b7c73d30b2e8c08d014a524dbff112cc'
                            icon='fas fa-sort-numeric-down'
                        >
                            <div className='description'>
                                <li>Designed Max Heap data structure to contribute to Heap Sort</li>
                                <li>Implemented iterative and recursive merge sort, quick sort, and heap sort</li>
                                <li>Observed performance of each sorting algorithm</li>
                            </div>
                        </ProjectCard>
                        <ProjectCard
                            languages='Python NLP ML BERT'
                            alt='Text Classification'
                            href='https://colab.research.google.com/drive/1nCFIALvjjAMr7qrz_OzUlXfQsk9KpYyo?usp=sharing'
                            icon='far fa-surprise'
                        >
                            <div className='description'>
                                <li>Utilized MobileBERT to tokenize text features and feed them into MobileBERT model to get output as a float for training model</li>
                                <li>Discover multi-label classification to output multi-emotions</li>
                                <li>Applied Logistic Regression to train dataset from&ensp;
                                    <a className='link' href='https://github.com/google-research/google-research/tree/master/goemotions'>GoEmotions</a>
                                </li>
                            </div>
                        </ProjectCard>
                    </div>
                    <div data-aos='fade-right' className='project-col'>
                        <ProjectCard
                            languages='C++ OpenCV'
                            alt='Height Estimator'
                            href='https://github.com/seanmiles/HeightEstimator'
                            icon='far fa-object-group'
                        >
                            <div className='description'>
                                <li>Utilized OpenCV library to detect a paper holded by a person in an image</li>
                                <li>Overlaid calculated height or an image on the paper</li>
                                <li>Collaborated with another 2 members to optimize, integrate, and test the program</li>
                            </div>
                        </ProjectCard>
                        <ProjectCard
                            languages='C++'
                            alt='Student Report MS'
                            href='https://github.com/tungxd96/Student-Report-Management-System'
                            icon='far fa-id-card'
                        >
                            <div className='description'>
                                <li>Developed C++ program to allow school facilities create student profile, add/modify/delete student records, view courses, and generate transcript.</li>
                                <li>Stored student name in hash table for improved search time complexity</li>
                            </div>
                        </ProjectCard>
                    </div>
                </div>
*/