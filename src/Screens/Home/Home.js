import React from 'react'
import Button from '../../Components/Button/Button';
import '../../Styles/Screen/Home/Home.css';

export default function Home() {
    return (
        <section id='Home' className='light'>
            <div className="home-content">
                <p>
                    I'm Tung Dinh, a software engineer based in Seattle, WA.
                </p>
                <Button
                    value="My Resume"
                    href='https://s3.us-east-2.amazonaws.com/tungxdinh.com/tung_resume+(2).pdf'
                />
            </div>
        </section>
    )
}
