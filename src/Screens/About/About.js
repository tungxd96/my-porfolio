import React, { useEffect, useState } from 'react';
import Title from '../../Components/Title/Title';
import '../../Styles/Screen/About/About.css';

const MY_INFO = {
    email: {
        value: 'tungxd301@gmail.com',
        order: 1
    },
    role: {
        value: 'Software Engineer',
        order: 2
    },
    phone: {
        value: '+1 (425) 229-8028',
        order: 3
    }
}
const COVER_IMG_HEIGHT = window.innerHeight / 4;
// const SIGNATURE_HEIGHT = window.innerHeight / 12;
const COVER_IMG_URL = 'https://s3.us-east-2.amazonaws.com/tungxdinh.com/tungxdinh-cover.jpeg';
const SIGNATURE_URL = 'https://s3.us-east-2.amazonaws.com/tungxdinh.com/signature_image.gif';
export default function About() {
    const [coverHeight, setCoverHeight] = useState(`${COVER_IMG_HEIGHT}px`);
    useEffect(() => {
        const ro = new ResizeObserver(entries => {
            for (const entry of entries) {
                const height = entry.target.clientHeight;
                setCoverHeight(height + 8);
            }
        })
        const infoCard = document.getElementById('info-card');
        ro.observe(infoCard);
    }, []);
    return (
        <section id='About' className='light'>
            <div className='wrapper'>
                <Title title='About' />
                <div className='about-p'>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Orci nulla pellentesque dignissim enim sit amet. Ultrices eros in cursus turpis massa tincidunt dui ut. Blandit massa enim nec dui nunc. Mi quis hendrerit dolor magna eget est lorem. In tellus integer feugiat scelerisque varius morbi. Sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Orci nulla pellentesque dignissim enim sit amet. Ultrices eros in cursus turpis massa tincidunt dui ut. Blandit massa enim nec dui nunc. Mi quis hendrerit dolor magna eget est lorem. In tellus integer feugiat scelerisque varius morbi. Sit amet mattis vulputate enim nulla aliquet porttitor lacus luctus.
                </div>
                <div style={{ marginTop: '3vw', position: 'relative' }}>
                    <img
                        src={COVER_IMG_URL}
                        alt="Tung X. Dinh"
                        width='100%'
                        height={coverHeight}
                        style={{ objectFit: 'cover', objectPosition: '50% 80%' }}
                    />
                    <div
                        id="info-card"
                        className='about-card'
                        style={{
                            right: '10vw',
                            top: '8.5vw'
                        }}
                    >
                        {Object.entries(MY_INFO)
                            .sort((a, b) => a[1].order - b[1].order)
                            .map(([infoKey, infoValue], index) => (
                                <div
                                    key={`${infoKey}-${index}`}
                                    style={{
                                        width: window.innerWidth / 5,
                                        ...index !== 0 ? { marginTop: '1.5vw' } : {}
                                    }}
                                >
                                    <div className='about-card-title'>{infoKey}</div>
                                    <div className='about-card-value'>{infoValue.value}</div>
                                </div>
                            ))}
                    </div>
                </div>
                <img alt='Tung Dinh' src={SIGNATURE_URL} height={`${coverHeight / 3}px`} style={{ marginTop: '2vw' }} />
            </div>
        </section>
    )
}
