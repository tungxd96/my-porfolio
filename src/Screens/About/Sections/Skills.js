import React from 'react'
import '../../../Styles/Screen/About/Sections/Skills.css'

export default function Skills() {
    // function drawLine(x1, y1, x2, y2, id) {
    //     const distance = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    //     const xMid = (x1 + x2) / 2;
    //     const yMid = (y1 + y2) / 2;
    //     const slopeRad = Math.atan2(y1 - y2, x1 - x2);
    //     const slopeDeg = (slopeRad * 180) / Math.PI;

    //     const line = document.getElementById(id);
    //     line.style.width = distance;
    //     line.style.top = yMid;
    //     line.style.left = xMid - (distance / 2);
    //     line.style.transform = `rotate(${slopeDeg}deg)`;
    // }

    // useEffect(() => {
    //     drawLine(550, 1470, 450, 1540, 'python-left');
    // }, []);

    return (
        <div className='skills'>
            <div className='heading'>
                <span>My Skills</span>
            </div>
            <ul>
                <li>Python</li>
                <li>C++</li>
                <li>Java</li>
                <li>HTML</li>
                <li>CSS</li>
                <li>JavaScript</li>
                <li>Node.js</li>
                <li>React.js</li>
                <li>SQL</li>
            </ul>
            {/* <div className='row'>
                <div>Python</div>
                <div>C++</div>
                <div>Java</div>
            </div>
            <div className='row'>
                <div>HTML</div>
                <div>CSS</div>
                <div>JavaScript</div>
            </div>
            <div className='row'>
                <div>Node.js</div>
                <div>React.js</div>
                <div>SQL</div>
            </div> */}
        </div>
    )
}
