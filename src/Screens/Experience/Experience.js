import React from 'react';
import Title from '../../Components/Title/Title';
import '../../Styles/Screen/Experience/Experience.css';
import { ICONS } from '../../Constants/Icons';
import WorkCard from '../../Components/Card/WorkCard';
import workMap from '../../Resources/works-map.json';
import { useHistory } from 'react-router-dom';

export default function Experience(props) {
    const history = useHistory();

    return (
        <section id='Experience' className='light'>
            <Title title='Work' />
            <div className='wrapper'>
                {Object.entries(workMap)
                    .sort((a, b) => a[1].order - b[1].order)
                    .map(([workKey, workValue], index) => {
                        const { key } = workValue || {};
                        return (
                            <WorkCard
                                key={`${workKey}-${index}`}
                                index={index + 1}
                                {...workValue}
                                icon={ICONS[key] || null}
                                style={index === 0 ? {} : { marginTop: '60px' }}
                                onClick={() => history.push(`/job/${key}`)}
                            />
                        );
                    })}
            </div>
        </section>
    )
}
