import React, { createElement, useEffect, useRef } from 'react';
import { useParams } from 'react-router-dom';
import Title from '../../../Components/Title/Title';
import { ICONS } from '../../../Constants/Icons';
import useWindowSize from '../../../Hooks/useWindowSize';
import { getWorkMapFromFile } from '../../../Utils/FileUtils';
import './Job.css';
import SC1 from '../../../Assets/Wireframes/SC1.png';
import SC2 from '../../../Assets/Wireframes/SC2.png';
import SC3 from '../../../Assets/Wireframes/SC3.png';
import SC4 from '../../../Assets/Wireframes/SC4.png';

const LOGO_SIZE = 240;
const WIREFRAME_SIZE = 300;

function Job(props) {
    const size = useWindowSize();

    //set the height of the body.
    useEffect(() => {
        setBodyHeight();
    }, [size.height]);

    //Set the height of the body to the height of the scrolling div
    const setBodyHeight = () => {
        document.body.style.height = `${scrollRef.current.getBoundingClientRect().height
            }px`;
    };

    // Configs
    const data = {
        ease: 0.1,
        current: 0,
        previous: 0,
        rounded: 0
    };

    // Run scrollrender once page is loaded.
    useEffect(() => {
        requestAnimationFrame(() => skewScrolling());
    });

    // Scrolling
    const skewScrolling = () => {
        if (!scrollRef.current || !scrollRef.current.style) return;
        //Set Current to the scroll position amount
        data.current = window.scrollY;
        // Set Previous to the scroll previous position
        data.previous += (data.current - data.previous) * data.ease;
        // Set rounded to
        data.rounded = Math.round(data.previous * 100) / 100;

        // Difference between
        // const difference = data.current - data.rounded;
        // const acceleration = difference / size.width;
        // const velocity = +acceleration;
        // const skew = velocity * 7.5;

        //Assign skew and smooth scrolling to the scroll container
        scrollRef.current.style.transform = `translate3d(0, -${data.rounded}px, 0)`;

        //loop vai raf
        requestAnimationFrame(() => skewScrolling());
    };

    const scrollRef = useRef();
    const { id } = useParams();
    const workMapFromFile = getWorkMapFromFile();
    const { title } = workMapFromFile[id] || {};
    return (
        <section id="Job" ref={scrollRef} style={styles.container}>
            <div style={styles.logo}>
                {createElement(ICONS[id], { width: LOGO_SIZE })}
            </div>
            <div style={styles.contentContainer}>
                <div style={styles.contentItemContainer}>
                    <Title title='Role' style={styles.titleMargin} />
                    <div className="bullet-points">
                        <div>{title}</div>
                    </div>
                </div>
                <div style={styles.contentItemContainer}>
                    <Title title='Description' style={styles.titleMargin} />
                    <div className="bullet-points">
                        <div>Implementing optimal solutions for frontend features using caching and batching mechanism</div>
                        <div>Developing, benchmarking, and optimizing backend endpoints with Lambda and DynamoDB</div>
                        <div>Handling deployment processes from native app to TestFlight and App Store and from serverless server to AWS</div>
                        <div>Collaborating with Tech Lead to manage tasks efficiently</div>
                        <div>Writing Node.js and Python scripts to automate file modification</div>
                        <div>Caching large number of images to AWS S3</div>
                        <div>Using CloudWatch to check Lambda functions logs</div>
                    </div>
                </div>
                <div style={styles.contentItemContainer}>
                    <Title title='Deliverables (Feature | Techniques | Responsible)' style={styles.titleMargin} />
                    <div>
                        <div style={{ margin: "64px 0" }}>Search Categories | Filtering + ElasticSearch | Full Stack</div>
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <img src={SC1} alt='SC1' width={WIREFRAME_SIZE} />
                            <img src={SC2} alt='SC2' width={WIREFRAME_SIZE} />
                            <img src={SC3} alt='SC3' width={WIREFRAME_SIZE} />
                            <img src={SC4} alt='SC4' width={WIREFRAME_SIZE} />
                        </div>
                    </div>
                    <div>
                        <div style={{ margin: "64px 0" }}>Gesture Bottom Modal Sheet | Modalize | Frontend</div>
                        <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                            <img src={SC1} alt='SC1' width={WIREFRAME_SIZE} />
                            <img src={SC2} alt='SC2' width={WIREFRAME_SIZE} />
                            <img src={SC3} alt='SC3' width={WIREFRAME_SIZE} />
                            <img src={SC4} alt='SC4' width={WIREFRAME_SIZE} />
                        </div>
                    </div>
                </div>

            </div>
        </section>
    );
}

const styles = {
    container: {
        justifyContent: 'flex-start',
        alignItems: 'center',
        height: '100%',
        width: '100%',
        minHeight: '100vh',
        backgroundColor: '#F5F5F5'
    },
    logo: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        padding: '150px 0'
    },
    contentContainer: {
        flexDirection: 'column',
        padding: '0 54px',
        fontSize: '20px',
        width: '100%'
    },
    contentItemContainer: {
        marginBottom: '96px',
    },
    titleMargin: {
        marginBottom: '32px',
        marginTop: '0'
    }
};

export default Job;