import React, { useEffect, useRef } from 'react';
import Experience from './Screens/Experience/Experience';
import Project from './Screens/Project/Project';
import 'aos/dist/aos.css';
import Navbar from './Components/Navbar/Navbar';
import useWindowSize from './Hooks/useWindowSize';
import {
  Route,
  BrowserRouter as Router,
  Switch
} from 'react-router-dom';
import Job from './Styles/Screen/Job/Job';
import About from './Screens/About/About';
import Footer from './Components/Footer/Footer';

function App() {
  return (
    <Router>
      <Switch>
        <Route path='/job/:id'>
          <Job />
        </Route>
        <Route path='/'>
          <MainPage />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;

const MainPage = () => {
  const size = useWindowSize();

  // Ref for parent div and scrolling div
  const appRef = useRef();
  const scrollRef = useRef();

  // Configs
  const data = {
    ease: 0.1,
    current: 0,
    previous: 0,
    rounded: 0
  };

  // Run scrollrender once page is loaded.
  useEffect(() => {
    requestAnimationFrame(() => skewScrolling());
  });

  //set the height of the body.
  useEffect(() => {
    setBodyHeight();
  }, [size.height]);

  //Set the height of the body to the height of the scrolling div
  const setBodyHeight = () => {
    document.body.style.height = `${scrollRef.current.getBoundingClientRect().height
      }px`;
  };

  // Scrolling
  const skewScrolling = () => {
    if (!scrollRef.current || !scrollRef.current.style) return;
    //Set Current to the scroll position amount
    data.current = window.scrollY;
    // Set Previous to the scroll previous position
    data.previous += (data.current - data.previous) * data.ease;
    // Set rounded to
    data.rounded = Math.round(data.previous * 100) / 100;

    // Difference between
    // const difference = data.current - data.rounded;
    // const acceleration = difference / size.width;
    // const velocity = +acceleration;
    // const skew = velocity * 7.5;

    //Assign skew and smooth scrolling to the scroll container
    scrollRef.current.style.transform = `translate3d(0, -${data.rounded}px, 0)`;

    //loop vai raf
    requestAnimationFrame(() => skewScrolling());
  };

  return (
    <div ref={appRef} id='container' className="App">
      <div ref={scrollRef} className='scroll'>
        <Navbar />
        <Experience />
        <Project />
        <About />
        <Footer />
      </div>
    </div>
  );
}