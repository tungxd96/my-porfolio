import CocoHealth from "../Assets/Icons/WorkIcons/CocoHealth";
import HealiAI from "../Assets/Icons/WorkIcons/HealiAI";
import PathfindingVisualizer from '../Assets/Images/Projects/PathfindingVisualizer.png';
import MazeGenerator from '../Assets/Images/Projects/MazeGenerator.png';
import SortingAlgorithm from '../Assets/Images/Projects/SortingAlgorithm.png';
import HeightEstimator from '../Assets/Images/Projects/HeightEstimator.png';

export const ICONS = {
    COCO_HEALTH: CocoHealth,
    HEALI_AI: HealiAI,
    PATHFINDING_VISUALIZER: PathfindingVisualizer,
    MAZE_GENERATOR: MazeGenerator,
    SORTING_ALGORITHM: SortingAlgorithm,
    HEIGHT_ESTIMATOR: HeightEstimator
}